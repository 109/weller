# weller

Weller controler GO lib

## Only Read temperature implement

Example:
``
package main

import (
	"log"
	"time"

	"fmt"

	"gitlab.com/109/weller"
)

func main() {
	w := weller.New("COM5")
	w.Open()
	w.EnableRemote()
	for {
		if c1, c2, err := w.ReadTmp(); err != nil {
			log.Fatal(err)
		} else {
			fmt.Println(c1, c2)
		}
		time.Sleep(time.Millisecond * 500)
	}

	w.DisableRemote()
	time.Sleep(time.Second * 30)
}
``
