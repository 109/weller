package weller

import (
	"log"

	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/tarm/serial"
)

//Rmoete
const (
	EnableRemote        = "remote1"
	EnableRemoteAndLock = "remote2"
	DisableRemote       = "remote0"
)

//Command or Command prefix
const (
	Status         = "Q1"
	ReadTmp        = "R"
	SetTmp         = "S%d"
	PreTmp1        = "T%d"
	PreTem2        = "U%d"
	SetBackTmp     = "L%d"
	SetBackTime    = "M%d"
	AutoOffsetTime = "N%d"
	OffsetTmp      = "O%d"
	Version        = "V1"
	WinTmpRange    = "W%d"
	ToolTyp        = "Y%d"
)

// ans
const (
	AnsTmp  = "R1%04d%cR2%04d%c"
	AnsUnit = "?1%04d%c"
)

//Transmit
const ()

//Unit ID
const (
	WX1 = iota + 1
	WX2
	WXD2
	WXA2
	WXD1
	WXA1
	WXR3
)

func checkSum() {

}

type Weller struct {
	serial *serial.Port
	port   string
	isOpen bool
	unitId int
	lock   sync.Mutex
}

func New(port string) *Weller {
	return &Weller{port: port}
}

func (w *Weller) Open() error {
	c := &serial.Config{Name: w.port, Baud: 1200, ReadTimeout: time.Second * 3}
	var err error
	w.serial, err = serial.OpenPort(c)
	if err != nil {
		return err
	}
	w.isOpen = true
	return nil
}

func (w *Weller) Close() {
	if w.isOpen {
		w.serial.Close()
	}
}

func (w *Weller) EnableRemote() error {
	w.lock.Lock()
	defer w.lock.Unlock()
	_, err := w.serial.Write([]byte(fmt.Sprintf(EnableRemote)))
	if err != nil {
		log.Fatal(err)
		return err
	}

	var str string
	var unitId int
	var checkSum byte
	for {
		b := make([]byte, 1)
		done := make(chan bool)
		go func() {
			_, e := w.serial.Read(b)
			if e != nil {
				log.Fatal(e)
			}
			done <- true
		}()
		select {
		case <-done:
		case <-time.After(3 * time.Second):
			return errors.New("enableRemote timeout!")
		}
		str = str + string(b)
		if _, e := fmt.Sscanf(str, AnsUnit, &unitId, &checkSum); e == nil {
			break
		}
		if len(str) > 11 {
			break
		}
	}

	fmt.Println(unitId, checkSum)
	//TODO check checksum
	w.unitId = unitId
	return nil
}

func (w *Weller) DisableRemote() error {
	w.lock.Lock()
	defer w.lock.Unlock()
	_, err := w.serial.Write([]byte(fmt.Sprintf(DisableRemote)))
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func (w *Weller) ReadTmp() (c1, c2 int, err error) {
	w.lock.Lock()
	defer w.lock.Unlock()
	_, err = w.serial.Write([]byte(fmt.Sprintf(ReadTmp)))
	if err != nil {
		log.Fatal(err)
		return
	}

	var str string
	var tmp1, tmp2 int
	var cs1, cs2 byte
	for {
		b := make([]byte, 1)
		_, err = w.serial.Read(b)
		if err != nil {
			log.Fatal(err)
			return
		}
		str = str + string(b)
		if _, e := fmt.Sscanf(str, AnsTmp, &tmp1, &cs1, &tmp2, &cs2); e == nil {
			break
		}
		if len(str) > 14 {
			break
		}
	}

	c1 = tmp1
	c2 = tmp2
	return
}
